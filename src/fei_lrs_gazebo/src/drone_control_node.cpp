#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/CommandTOL.h>

#include <math.h> 
    
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>

int lookAround(const sensor_msgs::PointCloud2ConstPtr& input_pc);
bool inRange(float low, float high, float x);

void chatterCallback(const sensor_msgs::PointCloud2ConstPtr&input_pc)
{
    sensor_msgs::PointCloud2 my_pc_; 
    my_pc_ = *input_pc; 
    int obstacle;
    
    obstacle = lookAround(input_pc);

    switch(obstacle) {
    case 0:
        printf("New\n");
        printf("Detekovana prekazka\n");
        break;
    case 1:
        printf("New\n");
        printf("Nic sa nedetekovalo\n");
        break;
    default:
        printf("Porucha\n");
    }
}

bool inRange(float low, float high, float x)
{
    return ((x-high)*(x-low) <= 0);
}


int lookAround(const sensor_msgs::PointCloud2ConstPtr& input_pc) {
    
    float factor2= 0, factor = 0;

    for (sensor_msgs::PointCloud2ConstIterator<float> it(*input_pc, "x"); it != it.end(); ++it) {
        if((it[0] != 'nan') && (it[1] != 'nan') && (it[2] != 'nan')){        
            // if(inRange(-0.25, 0.25, it[0]) && inRange(-0.25, 0.25, it[1])){
            bool first_range=(it[1]<=0.29) && (it[1]>=-0.29);
            bool second_range=(it[0]<=0.50) && (it[0]>=-0.50);
            if(first_range && second_range){
                if(it[2] <= 2){
                    factor++;
                }              
            } 
            factor2++;              
        }
    }

    float ratio=factor/factor2;

    if((ratio) <= 0.06){
        return 1;
    }else{
        return 0;
    }
    
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "drone_control_node");
    ros::NodeHandle nh;
    ros::Subscriber p2_sub_ = nh.subscribe("/fei_lrs_drone/stereo_camera/points2", 1, chatterCallback);
    ros::spin();

    return 0;
}
