## Disparity and pointcloud using gazebo 

#### 1. Package overview stereo_image_proc
- read overview and quick start -> http://wiki.ros.org/stereo_image_proc?distro=melodic#stereo_image_proc
#### 2. Download stereo_image_proc

```sudo apt-get install ros-melodic-stereo-image-proc```
#### 3. Test the setup for basic disparity output
- start FEI LRS gazebo environment 
- check all published topics with `rostopic` 
- refer to quick start -> quick start -> http://wiki.ros.org/stereo_image_proc?distro=melodic#stereo_image_proc.2Fcturtle.Quick_Start
    - run the package according to documentation
- visualize disparity and images

#### 4. Use RVIZ to vizualize pointcloud data
- launch rviz with command `rviz`
    - if rviz was not found refer to this guide and install it: http://wiki.ros.org/rviz/UserGuide
- display topic name -> /fei_lrs_drone/stereo_camera/points2 (or similar if you are using different workspace)
    - you need to create tf2 transformation from 'map' to 'left_camera_optical_frame'
    - to create transformation run this in another terminal
    ``` rosrun tf2_ros static_transform_publisher 0 0 0 0 0 0 1 map  left_camera_optical_frame ``` 

#### 5. Modify tf2 transformation
- modify tf2 transformation to achieve results as shown in result image
- refer to this package overview https://ocw.tudelft.nl/course-lectures/5-3-3-tf-tf2-ros-command-line-tools-static_transform_publisher/
- change translation or rotation (quaternion) parameters in command   ``` rosrun tf2_ros static_transform_publisher 0 0 0 0 0 0 1 map  left_camera_optical_frame ```   

#### Correct result in RVIZ:
![image](resources/pointcloud_rviz.png)


