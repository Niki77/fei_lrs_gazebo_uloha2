#  Map generator

1. Install dependencies. Use melodic or noetic according to your ROS installation.
```
sudo apt-get install ros-melodic-navigation
sudo apt-get install ros-melodic-octomap

```
2. Clone this git repo to your catkin workspace (~/fei_lrs_gazebo/src) : https://github.com/marinaKollmitz/gazebo_ros_2Dmap_plugin
3. Build catkin workspace.
4. Follow this guide to use map generation: https://github.com/marinaKollmitz/gazebo_ros_2Dmap_plugin/blob/master/README.md
