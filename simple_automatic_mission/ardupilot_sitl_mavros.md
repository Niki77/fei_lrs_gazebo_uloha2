This tutorial is aimed to test and gain some experience with ardupilot SITL environment before development of ROS control node.

## 0 Optional: Setup X-terminal-emulator 
X-terminal-emulator should ease work with multiple opened terminals. 


```
 sudo apt-get update -y 
 sudo apt-get install -y x-terminal-emulator 
 sudo apt-get install -y terminator #emulator from image

```

Possible setup:
![image](resources/setup_xterminal.png)
---

---
## 1 Ardupilot mission commands/services.
Tasks:

1. Arm UAV.
2. Takeoff.
3. Move drone between racks. ( use topic --> mavros/setpoint_raw/local )
- 3a. Just for fun try to fly throught shelves. 
4. Series of commands to achieve yaw rotation. 
5. Land in the default UAV location (spawn point in gazebo simulation).

Notes:

Ardupilot mission commands: https://mavlink.io/en/services/mission.html

Use rosservices for takeoff (Part 6 of document ardupilot_gazebo_setup.md). 

It is useful to use autocomplete feature in ubuntu, when calling rosservices (works for almost any command in command line), press double tab to complete the command or to list command variants.

Use rostopic publisher for guiding the drone or rqt.
```
rostopic pub  mavros/setpoint_raw/local mavros_msgs/PositionTarget "header:
  seq: 0                                                         
  stamp: {secs: 0, nsecs: 0}
  frame_id: ''
coordinate_frame: 1
type_mask: 0
position: {x: 1.0, y: 1.0, z: 1.0}
velocity: {x: 0.0, y: 0.0, z: 0.0}
acceleration_or_force: {x: .0, y: .0, z: .0}
yaw: 0.0
yaw_rate: 0.0" 
```


Target point: 
![image](resources/target_space.png)


---
## 2 Test QGroundControl

Tasks: 

1. Download and install. (https://ardupilot.org/copter/docs/common-install-gcs.html)
2. Turn on simulation environment. 
3. Wait for QGroundControl to connect to ardupilot SITL. 
4. Get familiar with the application, if you want to you can test various parameters.
5. Try to define simple mission, keep in mind that, space constrains of simulate indoor environment.

---
## Bonus 
Try these tutorial for working with ROS.

- https://www.youtube.com/watch?v=Qk4vLFhvfbI&list=PLLSegLrePWgIbIrA4iehUQ-impvIXdd9Q
- http://wiki.ros.org/ROS/Tutorials
